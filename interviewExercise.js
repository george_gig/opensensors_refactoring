const _ = require('lodash');

const relevantCategories = {category1: true, category2: false};
const ERRORS = {
  INVALID_PROJECT: 'Invalid or empty project!',
  UNKNOWN_TYPE: 'Unknown type!'
};

const categoryMap = {
  type1: 'category1',
  type2: 'category1',
  type3: 'category2'
};

module.exports.getDataByDeviceType = (project, types, allProjects = false, allCategories = false) => {
  return getValidProject(project)
    .then(project => getDevicesForProject(project, types))
    .then(devices => setDeviceTypes(devices))
    .then(([types, deviceCount]) => getDataByDeviceTypes(types, deviceCount))
    .then(([data, deviceTypes, totalCount]) => prepareResponse(data, deviceTypes, totalCount));
};

function getValidProject (project) {
  const DEFAULT_PROJECT = 'project1';
  let validProject = {};
  if ((project !== undefined) && (project !== null) && !_.isEmpty(project)) {
    validProject = project;
  } else {
    if (project !== DEFAULT_PROJECT) {
      validProject = project;
    } else {
      validProject = DEFAULT_PROJECT;
    }
  }
  return Promise.resolve(validProject);
}

function getDevicesForProject (project, types) {
  let devices = {};
  const PROJECTS_DB = {
    'project1': [
      {id: '123', type: 'type1'},
      {id: '234', type: 'type2'},
      {id: '345', type: 'type1'}],
    'project2': [
      {id: '345', type: 'type1'},
      {id: '456', type: 'type3'},
      {id: '567', type: 'type1'}]
  };
  devices = PROJECTS_DB[project];
  if (devices.length === 0 || devices === null || devices === undefined || _.isEmpty(devices)) {
    throw new Error(ERRORS.INVALID_PROJECT);
  } else {
    devices = devices.filter(device => types.includes(device.type));
    return Promise.resolve(devices);
  }
}

function setDeviceTypes (devices) {
  let types = _(devices)
    .filter(isRelevant)
    .groupBy(d => d.type)
    .mapValues(g => g.length)
    .value();
   // check if device is in relevant category
  function isRelevant (device) {
    return relevantCategories[categoryMap[device.type]];
  }
  return Promise.resolve([types, devices.length]);
}

function getDataByDeviceTypes (types, deviceCount) {
  const DATA_DB = {
    type1: 0.1,
    type2: 0.2,
    type3: 0.3
  };
  return Promise.resolve([_.pick(DATA_DB, Object.keys(types)), types, deviceCount]);
}

function prepareResponse (dataByType, groupSizeByType, totalSize) {
  let weightByTypes = getWeightByTypes(groupSizeByType, totalSize);
  let deviceTypes = _.keys(groupSizeByType);
  return Promise.resolve(deviceTypes.map(type => {
    return {
      type: type,
      data: dataByType[type] * weightByTypes[type]
    };
  }));
}

function getWeightByTypes (groupSizeByType, totalSize) {
  let weightByTypes = _.mapValues(groupSizeByType, (size) => (size / totalSize));
  return weightByTypes;
}
